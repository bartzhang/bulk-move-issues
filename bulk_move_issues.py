#!/usr/bin/env python3

import gitlab
import argparse

def move_issues(gl, fromproject, toproject, labels):
    moved_issues = []
    issues_to_move = []
    count = 0

    if labels:
        print("Moving issues with labels %s from %s to %s" % (labels, fromproject.id, toproject.id))
        issues_to_move = fromproject.issues.list(labels = labels, as_list=False)
    else:
        print("Moving all issues from %s to %s" % (fromproject.id, toproject.id))
        issues_to_move = fromproject.issues.list(as_list=False)
    for issue in issues_to_move:
        if issue.moved_to_id and issue.state == "closed":
            print("Already moved issue: %s, skipping" % issue.iid)
            continue
        count += 1
        try:
            issue.move(toproject.id)
            moved_issues.append(issue.iid)
        except Exception as e:
            print("Could not move issue %s: %s" % (issue.iid , e))
    print("Moved %s issues of %s identified" % (str(len(moved_issues)), str(count)))
    return moved_issues

parser = argparse.ArgumentParser(description='Bulk move issues')
parser.add_argument('token', help='API token able to create issues in the projects')
parser.add_argument('sourceproject', help='Project to move issues from')
parser.add_argument('targetproject', help='Project to move issues to')
parser.add_argument('--labels', help='Optional list of labels issues to move should have (comma separated)')
parser.add_argument('--gitlab', help='Optional GitLab instance url')
args = parser.parse_args()


gitlaburl = args.gitlab.strip("/") + "/" if args.gitlab else "https://gitlab.com/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

sourceproject = None
try:
    sourceproject = gl.projects.get(args.sourceproject)
except Exception as e:
    print("Could not get source project: {0}".format(e))
    exit(1)
targetproject = None

try:
    targetproject = gl.projects.get(args.targetproject)
except Exception as e:
    print("Could not get target project: {0}".format(e))
    exit(1)

labels = args.labels.split(",") if args.labels else []
if labels:
    print("Getting target project labels to avoid data loss")
    targetproject_labels = [label.name for label in targetproject.labels.list(all=True)]
    for label in labels:
        if label not in targetproject_labels:
            print("Error: issue label not in target project labels: %s" % label)
            print("Label will not be able to be transferred")
            print("Stopping to prevent data loss, make sure same labels are available to both projects")
            exit(1)

moved_issues = move_issues(gl, sourceproject, targetproject, labels)
with open("moved_issues","w") as reportfile:
    reportfile.write("Source: %s\n" % sourceproject.id)
    reportfile.write("Target: %s\n" % targetproject.id)
    reportfile.write("Issues:\n")
    for issue_iid in moved_issues:
        reportfile.write(str(issue_iid)+"\n")
